# Petclinic Pipeline

This application makes use of https://github.com/spring-projects/spring-petclinic and deploys it into AWS using a Jenkins pipeline and RDS.

The project creates Dev and Prod environments, but can be configured to add others like Test or QA or Staging.
