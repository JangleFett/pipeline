output "bastion_id" {
  value = aws_instance.bastion.id
}

output "bastion_pub_ip" {
  value = aws_instance.bastion.public_ip
}

output "bastion_priv_ip" {
  value = aws_instance.bastion.private_ip
}

output "jenkins_id" {
  value = aws_instance.jenkins.id
}

output "jenkins_pub_ip" {
  value = aws_instance.jenkins.public_ip
}

output "jenkins_priv_ip" {
  value = aws_instance.jenkins.private_ip
}

output "jenkins_eip_id" {
  value = aws_eip.jenkins.id
}

output "jenkins_eip" {
  value = aws_eip.jenkins.public_ip
}

output "jenkins_dns_name" {
  value = aws_route53_record.jenkins.fqdn
}

output "docker_registry_ip" {
  value = aws_instance.registry.private_ip
}

output "docker_registry_id" {
  value = aws_instance.registry.id
}
