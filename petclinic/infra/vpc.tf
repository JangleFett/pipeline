module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = var.project
  cidr = var.vpc_cidr

  azs             = ["${var.region}a", "${var.region}b"]
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  create_igw              = true
  map_public_ip_on_launch = false

  enable_nat_gateway  = true
  single_nat_gateway  = false
  reuse_nat_ips       = true
  external_nat_ip_ids = aws_eip.nat.*.id

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name        = "${var.project}-${terraform.workspace}"
    Environment = terraform.workspace
  }

  private_subnet_tags = {
    Name        = "${var.project}-${terraform.workspace}-private"
    Environment = terraform.workspace
  }

  public_subnet_tags = {
    Name        = "${var.project}-${terraform.workspace}-public"
    Environment = terraform.workspace
  }
}

resource "aws_eip" "nat" {
  count = 2
  vpc   = true
}
