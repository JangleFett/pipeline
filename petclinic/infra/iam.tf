resource "aws_iam_role" "jenkins_role" {
  name = "${var.project}-${terraform.workspace}-jenkins-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "jenkins_role" {
  count      = length(var.iam_policies_jenkins)
  role       = aws_iam_role.jenkins_role.name
  policy_arn = var.iam_policies_jenkins[count.index]
}
