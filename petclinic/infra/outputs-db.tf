output "db-hostname" {
  value = aws_db_instance.petclinic.address
}

output "db-endpoint" {
  value = aws_db_instance.petclinic.endpoint
}

output "db-id" {
  value = aws_db_instance.petclinic.id
}

output "db-name" {
  value = aws_db_instance.petclinic.name
}

output "db-user" {
  value = aws_db_instance.petclinic.username
}

output "db-pass" {
  value = aws_db_instance.petclinic.password
}
