output "vpc_id" {
  value = module.vpc.vpc_id
}

output "main_route_table" {
  value = module.vpc.vpc_main_route_table_id
}

output "public_subnets" {
  value = module.vpc.public_subnets
}

output "private_subnets" {
  value = module.vpc.private_subnets
}

output "natgw_ids" {
  value = module.vpc.natgw_ids
}

output "nat_eips" {
  value = module.vpc.nat_public_ips
}

output "igw_id" {
  value = module.vpc.nat_public_ips
}
