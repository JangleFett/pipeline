resource "aws_security_group" "jenkins" {
  name        = "${var.project}-${terraform.workspace}-jenkins-sg"
  description = "Allow inbound for Jenkins"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "Jenkins Web UI"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = var.office_cidr
  }

  ingress {
    description     = "Jenkins SSH from Bastion"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project}-${terraform.workspace}-jenkins-sg"
    Environment = terraform.workspace
  }
}

resource "aws_security_group" "bastion" {
  name        = "${var.project}-${terraform.workspace}-bastion-sg"
  description = "Allow inbound for Bastion"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.office_cidr
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project}-${terraform.workspace}-bastion-sg"
    Environment = terraform.workspace
  }
}

resource "aws_security_group" "webapp_public" {
  name        = "${var.project}-${terraform.workspace}-application-sg"
  description = "Allow inbound for Application"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "Web Port"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project}-${terraform.workspace}-webapp-pubsg"
    Environment = terraform.workspace
  }
}

resource "aws_security_group" "all_internal" {
  name        = "${var.project}-${terraform.workspace}-internal-sg"
  description = "Allow inbound for Application Internal"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "Internal Traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project}-${terraform.workspace}-internal-sg"
    Environment = terraform.workspace
  }
}
