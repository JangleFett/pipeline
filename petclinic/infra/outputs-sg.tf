output "jenkins-sg" {
  value = aws_security_group.jenkins.name
}

output "jenkins-sg-id" {
  value = aws_security_group.jenkins.id
}

output "bastion-sg" {
  value = aws_security_group.bastion.name
}

output "bastion-sg-id" {
  value = aws_security_group.bastion.id
}

output "webapp-sg" {
  value = aws_security_group.webapp_public.name
}

output "webapp-sg-id" {
  value = aws_security_group.webapp_public.id
}

output "internal-sg" {
  value = aws_security_group.all_internal.name
}

output "internal-sg-id" {
  value = aws_security_group.all_internal.id
}
