resource "aws_instance" "bastion" {
  ami                         = var.ami[var.region]
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.generated_key.key_name
  vpc_security_group_ids      = [aws_security_group.bastion.id]
  associate_public_ip_address = true
  subnet_id                   = element(module.vpc.public_subnets, 0)

  user_data = <<EOF
  #!/bin/bash -xv
  amazon-linux-extras enable lamp-mariadb10.2-php7.2
  yum -y install nc git mariadb

  echo "${tls_private_key.sshkey.private_key_pem}" >/home/ec2-user/.ssh/id_rsa
  chmod 600 /home/ec2-user/.ssh/id_rsa
  chown ec2-user:ec2-user /home/ec2-user/.ssh/id_rsa
  EOF

  tags = {
    Name        = "${var.project}-${terraform.workspace}-bastion"
    Environment = terraform.workspace
  }
}
