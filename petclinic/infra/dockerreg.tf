resource "aws_instance" "registry" {
  ami                         = var.ami[var.region]
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.generated_key.key_name
  vpc_security_group_ids      = [aws_security_group.all_internal.id]
  associate_public_ip_address = false
  subnet_id                   = element(module.vpc.private_subnets, 0)

  user_data = <<EOF
  #!/bin/bash -xv
  yum -y install docker
  systemctl enable docker
  systemctl start docker
  docker run -itd -p 5000:5000 --name dockerreg -v /var/lib/registry:/var/lib/registry --restart=always registry:2
  EOF

  tags = {
    Name        = "${var.project}-${terraform.workspace}-registry"
    Environment = terraform.workspace
  }
}
