output "ssh_private_key_pem" {
  value = tls_private_key.sshkey.private_key_pem
}

output "ssh_public_key_pem" {
  value = tls_private_key.sshkey.public_key_pem
}

output "ssh_public_key_openssh" {
  value = tls_private_key.sshkey.public_key_openssh
}
