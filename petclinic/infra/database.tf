resource "aws_db_subnet_group" "petclinic" {
  name       = "${var.project}-${terraform.workspace}-db"
  subnet_ids = module.vpc.private_subnets

  tags = {
    Name        = "${var.project}-${terraform.workspace}-db"
    Environment = terraform.workspace
  }
}

resource "aws_db_instance" "petclinic" {
  identifier                = "${var.project}-${terraform.workspace}-db"
  allocated_storage         = 5
  max_allocated_storage     = 10
  engine                    = "mysql"
  engine_version            = "5.7"
  instance_class            = "db.t2.micro"
  monitoring_interval       = 0
  apply_immediately         = true
  name                      = "petclinic"
  username                  = "petclinic"
  password                  = "secret123"
  parameter_group_name      = "default.mysql5.7"
  backup_window             = "00:01-02:00"
  copy_tags_to_snapshot     = true
  db_subnet_group_name      = aws_db_subnet_group.petclinic.name
  delete_automated_backups  = true
  final_snapshot_identifier = "${var.project}-${terraform.workspace}-db"
  skip_final_snapshot       = true
  maintenance_window        = "Sun:02:15-Sun:02:59"
  multi_az                  = true
  publicly_accessible       = false
  snapshot_identifier       = var.db_backup == false ? null : var.db_backup
  vpc_security_group_ids    = [aws_security_group.all_internal.id]
  tags = {
    Name        = "${var.project}-${terraform.workspace}-db"
    Environment = terraform.workspace
  }
}
