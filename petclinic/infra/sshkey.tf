resource "tls_private_key" "sshkey" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = "${var.project}-${terraform.workspace}"
  public_key = tls_private_key.sshkey.public_key_openssh
}

# Store the private key
resource "local_file" "sshkey" {
  content         = tls_private_key.sshkey.private_key_pem
  filename        = "../secrets/sshkey.pem"
  file_permission = "0600"
}

resource "aws_s3_bucket_object" "sshkey" {
  bucket = var.bucketname
  key    = "env:/${terraform.workspace}/sshkey.pem"
  source = "../secrets/sshkey.pem"
}
