resource "aws_instance" "jenkins" {
  depends_on                  = [aws_instance.registry]
  ami                         = var.ami[var.region]
  instance_type               = "t3a.large"
  key_name                    = aws_key_pair.generated_key.key_name
  vpc_security_group_ids      = [aws_security_group.jenkins.id]
  associate_public_ip_address = true
  subnet_id                   = element(module.vpc.public_subnets, 0)
  iam_instance_profile        = aws_iam_instance_profile.jenkins_inst_profile.name

  user_data = <<EOF
  #!/bin/bash -xv
  amazon-linux-extras enable epel
  amazon-linux-extras enable ansible2
  yum -y install epel-release ansible git python-pip python3-pip
  pip install python-jenkins lxml jenkins
  cd /tmp
  git clone --recursive https://github.com/stevshil/jenkins-local.git
  cd jenkins-local
  export environment="${terraform.workspace}"
  ansible-playbook -i environments/"${terraform.workspace}" create.yml
  EOF

  tags = {
    Name        = "${var.project}-${terraform.workspace}-jenkins"
    Environment = terraform.workspace
  }
}

resource "aws_iam_instance_profile" "jenkins_inst_profile" {
  name = "${var.project}-${terraform.workspace}-jenkins-inst-profile"
  role = aws_iam_role.jenkins_role.name
}

resource "aws_eip" "jenkins" {
  instance = aws_instance.jenkins.id
  vpc      = true

  tags = {
    Name        = "${var.project}-${terraform.workspace}-jenkins"
    Environment = terraform.workspace
  }
}

resource "aws_route53_record" "jenkins" {
  zone_id = var.dns_zone_id
  name    = "jenkins.${var.project}.${terraform.workspace}.${var.dns_suffix}"
  type    = "A"
  ttl     = "60"
  records = [aws_eip.jenkins.public_ip]
}
